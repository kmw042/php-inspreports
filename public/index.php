
<?php
/*
 * Generate 1 Field Inspection Report
 * 
 * Web page to select a report number from the database
 * and return a PDF doc
 * 
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Generate PDF Field Inspection Report</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <script>
function showUser(str) {
    if (str == "") {
        document.getElementById("reportID").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("reportID").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../src/HTML_Inspection_report.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>
    </head>
    <body>
        <div class="container" style="padding-top:50px">
            <h2>Generate Inspection Report</h2>
            <form class="form-inline" method="post" action="../src/generateInspectionReport.php">
                <?php include '../src/reportSelectList.php' ?>
                <button type="submit" id="pdf" name="generate_pdf" class="btn btn-primary"><i class="fa fa-pdf"" aria-hidden="true"></i>
                    Generate PDF</button>
            </form>
            <br>
<div id="reportID"><b></b></div>

       
    </div>
</body>
</html>