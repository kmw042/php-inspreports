<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit64bd9eec5658502983341e280ef774b6
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Fpdf\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Fpdf\\' => 
        array (
            0 => __DIR__ . '/..' . '/fpdf/fpdf/src/Fpdf',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit64bd9eec5658502983341e280ef774b6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit64bd9eec5658502983341e280ef774b6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
