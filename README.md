## Generates a Field Inspection Report as a PDF ##
_________________________________________________________

### A Field Inspection Report consists of: ###

### - A header containing the customer info, report numnber and the data from the most recent inspection: datetime, inspector name, and a jpg image of the inspector's signature ##

### - A detail section containing a line for each inspection in the field including the datetime, the inspector's notes, & the inspector's ID ##

### DO NOT USE IN PRODUCTION - DB IS NOT SECURE/HARDENED ###

### Use: ###
### - link the public dir needs into your DOC_ROOT dir ###
### - run .sql files to create db and populate with sample data ###
### - web page demo: public/index.php ###

### Requires: php 7.0 or newer ###
### 

kmw042@gmail.com