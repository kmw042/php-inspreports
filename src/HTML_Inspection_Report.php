

  
<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>

<?php
include_once("db/Inspection_Report.php");
//declare(strict_types = 1);

//namespace kmwFIR;
$q = intval($_GET['q']);
//init data and set vars
        //$pdfIni = parse_ini_file('../config/constants.ini');
if ($q != null)
        $inspReport = new kmwFIR\db\Inspection_Report($q);



echo "<table>
<tr>
<th>Customer</th>
</tr>";
foreach ($inspReport->getMasterData() as $colname => $column) {
    echo "<tr>";
    echo "<td>" . $column . "</td>";
//    echo "<td>" . $column['name'] . "</td>";
//    echo "<td>" . $column['address_line1'] . "</td>";
//    echo "<td>" . $column['address_line2'] . "</td>";
//    echo "<td>" . $column['city'] . "</td>";
    echo "</tr>";
}
echo "</table>";
echo "<br>";
echo "<table>
<tr>";
foreach ($inspReport->getDetailCols() as $heading => $size) {
            echo "<th>" . $size[1]. '</th>';
        }
echo "</tr>";


        foreach ($inspReport->getDetailData() as $row) {
            echo "<tr>";
            echo "<td>" . $row['update_time'] . "</td>";
    echo "<td>" . $row['notes'] . "</td>";
    echo "<td>" . $row['username'] . "</td>";
 
    echo "</tr>";
        }
        echo "</table>";
?>
</body>
</html>



