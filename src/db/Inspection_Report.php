<?php

declare(strict_types = 1);

namespace kmwFIR\db;

/*
 *  Class Inspection_Report
 *
 *  This class represents the master & detail data for a Field Inspection Report
 *
 *  @input - $rptNumber	- A report ID from table insp_header.id
 *
 *  kmw042 ©2018
 */

class Inspection_Report {

    // class vars
    private $mysqliInstance = null;
    private $reportNum = 0;
    private $masterData = null;
    private $detailData = null;
    private $detailCols = null;

    /*
     *  Default constructor
     *  @input - $rptNumber	- A report ID from table insp_header.id
     */

    public function __construct(int $rptNumber) {

        //get the connection info
        $ini = parse_ini_file('../db.ini');

        //instantiate the db instance

        try {
            $this->mysqliInstance = new \mysqli($ini['db_host'], 
                    $ini['db_user'], $ini['db_password'], $ini['db_name']);
        } catch (Exception $e) {
            echo 'ERROR:' . $e->getMessage();
            die();
        }

        //load data and set vars
        $this->load($rptNumber);
    }

    /*
     * Public functions
     */


    /*
     *  Release db resources
     */

    public function close(): void {
        $this->mysqliInstance->close();
    }

    public function getReportNumber(): int {
        return $this->reportNum;
    }

    public function getMasterData(): array {
        return $this->masterData;
    }

    public function getDetailCols(): array {
        return $this->detailCols;
    }

    public function getDetailData(): array {
        return $this->detailData;
    }

    /*
     * Private functions
     */

    /*
     * Execute queries and set report data
     *  @input - $rptNumber	- Report ID from table insp_header.id
     */

    private function load(int $rptNumber) {

        try {
            $this->setReportNumber($rptNumber);
            $this->setMasterData($this->doMasterQuery($this->reportNum));
            $this->setDetailData($this->doDetailQuery($this->reportNum));
        } catch (Exception $e) {
            echo 'ERROR:' . $e->getMessage();
            die($e->getMessage());
        }
        $this->detailCols = array('update_time' => [50, 'Date Time'], 
            'notes' => [240, 'Notes'], 'username' => [20, 'Inspector']);
    }

    function doMasterQuery($reportNumber) {

        $masterQuery = "select c.name, c.address_line1, c.address_line2, c.city,
                          c.state, c.country, c.zip, ih.id as 'Report Number', 
                          id.update_time as Updated,
                          u.username as 'By', u.sig_file as Signature
                        from FIReports.customer c,
                          FIReports.insp_header ih
                        left join insp_detail id on ih.id = id.header_id
                        join user u on id.insp_id = u.id
                        where ih.id = $reportNumber and ih.customer_id = c.id
                        order by id.update_time DESC
                        limit 1";
        $masterResult = $this->mysqliInstance->query($masterQuery);
        $masterResultsArray = $masterResult->fetch_array(MYSQLI_ASSOC);
        $masterResult->free();
        return $masterResultsArray;
    }

    function doDetailQuery($reportNumber) {

        $detailQuery = "select id.update_time as update_time, id.notes as notes,
                          u.username as username
                        from insp_detail id, user u
                        where id.header_id = $reportNumber and
                          id.insp_id = u.id order by update_time asc";
        $detailResult = $this->mysqliInstance->query($detailQuery);
        $detailResultsArray = $detailResult->fetch_all(MYSQLI_ASSOC);
        $detailResult->free();
        return $detailResultsArray;
    }

    private function setReportNumber(int $reportNumber): void {
        $this->reportNum = $reportNumber;
    }

    private function setMasterData(array $master): void {
        $this->masterData = $master;
    }

    private function setDetailCols(array $details): void {
        $this->detailCols = $details;
    }

    private function setDetailData(array $details): void {
        $this->detailData = $details;
    }

}

//end class Inspection_Report