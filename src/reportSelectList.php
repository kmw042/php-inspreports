<?php

declare(strict_types = 1);
/*
 *  This utility file generates an html select list of valid
 *  Field Inspection Report IDs
 *
 *  kmw042 ©2018
 */

//get the connection info
$ini = parse_ini_file('../db.ini');

//instantiate the db instance
mysqli_report(MYSQLI_REPORT_STRICT);
try {
    $conn = new mysqli($ini['db_host'], $ini['db_user'], $ini['db_password'], 
            $ini['db_name']);
} catch (Exception $e) {
    echo 'ERROR:' . $e->getMessage();
    die();
}

//load the data
$sql = "SELECT insp_header.id  as reportID FROM insp_header join insp_detail on "
        . "insp_header.id = insp_detail.id order by insp_header.id asc";
$result = mysqli_query($conn, $sql);

//output html
echo "<select name='reportID' . onchange='showUser(this.value)'>";
while ($row = mysqli_fetch_array($result)) {
    echo "<option value='" . $row['reportID'] . "'>" . $row['reportID'] 
          . "</option>";
}
echo "</select>";

