<?php

declare(strict_types = 1);

namespace kmwFIR;

/*
 *  Class PDF_Inspection_Report
 *
 *  This class represents the master & detail data for a Field Inspection Report
 *
 *  @input - $rptNumber	- A report ID from table insp_header.id
 *
 *  kmw042 ©2018
 */


include_once("lib/PDF_MC_Table.php");
include_once("db/Inspection_Report.php");

class PDF_Inspection_Report extends lib\PDF_MC_Table {

    // data model
    public $inspReport;
    //config settings
    public $pdfIni;

    /*
     *  Default constructor
     *  @input - $rptNumber	- A report ID from table insp_header.id
     */

    public function __construct(int $rptNumber = null) {

        //init data and set vars
        $this->pdfIni = parse_ini_file('../config/constants.ini');
        $this->inspReport = new db\Inspection_Report($rptNumber);

        //init new PDF
        parent::__construct('L', 'mm', 'Legal');
    }

    /*
     *  @override header - Create custom header with customer info, report
     *    number and datetime + username + signature of the most recent inspector
     *
     *   called by fpdf->AddPage()
     */

    function Header() {

        if ($this->page == 1) {   //only show the header on the first page
            // Logo
            $this->Image($this->pdfIni["report_logo"], 0, -1, 70);

            // Company name
            $this->SetFont($this->pdfIni["report_font"], 'I', 24);
            // Move to the right
            $this->Cell(40);
            $this->Cell(80, 10, $this->pdfIni["company_name"], 1, 0, 'C');

            // Title
            $this->SetFont($this->pdfIni["report_font"], 'B', 24);
            $this->Cell(40);
            $this->Cell(130, 20, 'Field Inspection Report', 0, 1, 'R');

            // label
            $this->SetFont($this->pdfIni["report_font"], 'B', 14);
            $this->Cell(20, 15, 'Customer:', 0);

            // Customer and report data - 2 col display
            $this->Ln();

            // store the current Y pos to use for 2nd header column
            $currY = $this->GetY();

            foreach ($this->inspReport->getMasterData() as $colname => $column) {

                if (strcmp($colname, 'Report Number') == 0) { // report number
                    $this->SetFont($this->pdfIni["report_font"], 'B', 20);
                    $this->SetY($currY);
                    $this->SetX(180);
                    $this->Cell(60, 20, $colname, 0);
                    $this->Cell(0, 20, $column, 0);
                } elseif (strcmp($colname, 'Updated') == 0) { // updated
                    $this->SetFont($this->pdfIni["report_font"], '', 16);
                    $this->SetX(180);
                    $this->Cell(35, 8, $colname, 0);
                    $this->Cell(0, 8, $column, 0);
                } elseif (strcmp($colname, 'By') == 0) { // inspector
                    $this->SetFont($this->pdfIni["report_font"], '', 16);
                    $this->SetX(180);
                    $this->Cell(35, 8, 'By', 0);
                    $this->Cell(0, 8, $column, 0);
                } elseif (strcmp($colname, 'Signature') == 0) { // signature
                    $this->SetFont($this->pdfIni["report_font"], '', 16);
                    $this->SetX(180);
                    $this->Cell(5, 15, 'Signature', 0);
                    $this->Cell(0, 15, 
                            $this->Image($this->pdfIni["signature_file_loc"] .
                                    $column, 215, $this->GetY(), 100, 15), 0);
                } else {                                    //customer data
                    $this->SetFont($this->pdfIni["report_font"], '', 14);
                    $this->Cell(25, 6, $column, 0);
                }
                $this->Ln();
            }

            $this->Ln(20);
        } // end if
    }

    /*
     *  @override footer - Create custom footer with page no/page count
     *
     *   called by fpdf->AddPage()
     */

    function Footer() {
        $this->SetFont($this->pdfIni["report_font"], 'I', 9);

        // Page number
        // position 1.5 cm from bottom
        $this->SetY(-15);
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function CreatePDFReport() {

        $this->AddPage('L');
        $this->AliasNbPages();

        // detail headings
        $this->SetFont($this->pdfIni["report_font"], 'B', 14);
        foreach ($this->inspReport->getDetailCols() as $heading => $size) {
            $this->Cell($size[0], 12, $size[1], '0');
        }

        // detail data
        $this->SetFont($this->pdfIni["report_font"], '', 12);
        $this->SetWidths(array(50, 240, 40));  //set the data column widths
        $this->Ln();

        // insert the detail data one row at a time
        foreach ($this->inspReport->getDetailData() as $row) {
            $this->Row(array($row['update_time'], $row['notes'], 
                $row['username']));
        }
    }

}

//end class Inspection_Report


