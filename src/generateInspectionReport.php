<?php

declare(strict_types = 1);
/*
 *  This utility file responds to a POST request with a PDF version of a
 *  Field Inspection Report
 *
 *  Use reportSelectList.php to generate a valid list of report numbers
 *
 *  @input - $rptNumber	- A report ID from table insp_header.id
 *
 *  kmw042 ©2018
 */

include_once 'PDF_Inspection_Report.php';

//get the report number
$reportNum = $_POST['reportID'];

//create the PDF in memory
$pdf = new kmwFIR\PDF_Inspection_Report((int) $reportNum);
$pdf->CreatePDFReport();

//show it to the user
$pdf->Output();




